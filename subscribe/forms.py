from django import forms

class subscribe_forms(forms.Form):
    nama = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control'}))
    password = forms.CharField(min_length=6, max_length=12, widget=forms.PasswordInput(attrs={'class':'form-control'}))