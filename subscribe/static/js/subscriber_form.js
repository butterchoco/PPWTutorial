$(document).ready( function() {

    function unsubs(id){
        console.log(true);
        return function() {
            console.log("test");
            var email = $("#email"+id).text();
            $.ajax({
                method: "POST",
                url: "/subscribe/delete/",
                data:{
                    "email": email
                },
                render: function(result) {
                    $("#row"+id).remove();
                }
            });
        }
    }

    function ajaxSubs(){
        $.ajax({
            method: "GET",
            url: '/subscribe/list_subscriber',
            success: function(result) {
                if(result.length > 0) {
                    for(var i = 0; i < result.length; i++) {
                        var nama = result[i]['nama'];
                        var email = result[i]['email'];
                        var temp = "<tbody>";
                        temp += "<tr id='row"+i+"' class='success'><td>"+ nama + "</td>";
                        temp += "<td id='email"+ i + "'>" + email + "</td>";
                        temp += "<td><button id='unsubscribe"+ i +"' class='btn btn-danger'>Unsubscribe</button></td></tr>"
                        temp += "</tbody>"
                        $(document).on("click", "#unsubscribe"+i, unsubs(i));
                        $("#list_subscriber").append(temp)
                    }
                }
            }
        });
    }

    $(window).load( function() {
        ajaxSubs()
    })
        
    
    $("input").keyup(function () {
        email = $('#id_email').val();
        console.log(email)
        $.ajax({
            method: "POST",
            url: '/subscribe/validate/',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken) {
                    $("#alert").html('<div class="alert alert-danger" role="alert">Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain !</div>')
                    $('#submit').prop("disabled", true);
                } else {
                    $(".alert").remove()
                }
                if (($('#id_nama').val() == '' || $('#id_email').val() == '' || $('#id_password').val() == '') || data.is_taken){
                    $('#submit').prop("disabled", true);
                } else {
                    $('#submit').removeAttr('disabled');
                }
            }
        });
    });

    $("form").submit( function() {
        nama = $('#id_nama').val();
        email = $('#id_email').val();
        password = $('#id_password').val();
        $.ajax({
            method: "POST",
            url: '/subscribe/add_subscriber/',
            data: {
                'nama': nama,
                'email': email,
                'password': password
            },
            dataType: 'json',
            success: function (data) {
                console.log(true)
                $("#alert").html('<div class="alert alert-success" role="alert">Data berhasil disimpan !</div>');
                $(".alert-success").animate({opacity:"0"}, 2500)
                $(".alert-success").animate({height:"0"}, 1000)
                $('#id_nama').val("");
                $('#id_email').val("");
                $('#id_password').val("");
            }
        });
        event.preventDefault();
    })
});