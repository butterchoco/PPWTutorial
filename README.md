[![pipeline status](https://gitlab.com/ahmad364/PPWTutorial/badges/master/pipeline.svg)](https://gitlab.com/ahmad364/PPWTutorial/commits/master) [![coverage report](https://gitlab.com/ahmad364/PPWTutorial/badges/master/coverage.svg)](https://gitlab.com/ahmad364/PPWTutorial/commits/master)

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/ahmad364/PPWTutorial.git)
# Repository Instruksi Lab

CSGE602022 - Perancangan & Pemrograman Web @
Fakultas Ilmu Komputer - Universitas Indonesia, Semester Genap 2017/2018

Nama	: Ahmad Supriyanto
NPM	: 1706075016
Class	: C
Hobby	: Editing Video
Repo	: https://gitlab.com/ahmad364/PPWTutorial.git
Apps	: http://ppw-c-belajarfilm.herokuapp.com/