"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('/', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('/', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  path('/blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import path
from django.contrib import admin
import home.urls as home
import about.urls as about
import schedule.urls as schedule
import library.urls as library
import subscribe.urls as subscribe
from django.views.generic.base import RedirectView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include(home, namespace='home')),
    path('about/', include(about, namespace='about')),
    path('schedule/', include(schedule, namespace='schedules')),
    path('library/', include(library, namespace='library')),
    path('subscribe/', include(subscribe, namespace='subscribe')),
    path('', RedirectView.as_view(url='/home/')),
]
