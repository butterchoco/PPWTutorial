$(window).load(function() {
   $('.preloader').fadeOut('slow');
});

var y = true;
function theme(){
    y = y ^ true;
    if (y) {    
        document.body.style.backgroundColor = "white";
        var list1 = document.getElementsByTagName("P");
        var i;
        for(i = 0; i < list1.length; i++){
            list1[i].style.color = "black";
        }
        var list2 = document.getElementsByTagName("h3");
        for(i = 0; i < list2.length; i++){
            list2[i].style.color = "black";
        }
        var list3 = document.getElementsByTagName("h2");
        for(i = 0; i < list3.length; i++){    
            list3[i].style.color = "black";
        }
        var list4 = document.getElementsByTagName("h1");
        for(i = 0; i < list4.length; i++){
            list4[i].style.color = "black";
        }
    } else {
        document.body.style.backgroundColor = "#222222";
        var list1 = document.getElementsByTagName("P");
        var i;
        for(i = 0; i < list1.length; i++){
            list1[i].style.color = "white";
        }
        var list2 = document.getElementsByTagName("h3");
        for(i = 0; i < list2.length; i++){
            list2[i].style.color = "white";
        }
        var list3 = document.getElementsByTagName("h2");
        for(i = 0; i < list3.length; i++){    
            list3[i].style.color = "white";
        }
        var list4 = document.getElementsByTagName("h1");
        for(i = 0; i < list4.length; i++){
            list4[i].style.color = "white";
        }
    }
}