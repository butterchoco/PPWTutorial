$(window).load(function() {
   $('.preloader').fadeOut('slow');
});

$(document).ready(function(){
        $("#accordion1").click(function(){
                $("#panel1").slideToggle("slow");
        });
});

$(document).ready(function(){
        $("#accordion2").click(function(){
                $("#panel2").slideToggle("slow");
        });
});

$(document).ready(function(){
        $("#accordion3").click(function(){
                $("#panel3").slideToggle("slow");
        });
});

var y = true;
function theme(){
    y = y ^ true;
    if (y) {    
        document.body.style.backgroundColor = "white";
        document.getElementById("accordion1").style.backgroundColor = "#eeeeee";
        document.getElementById("panel1").style.backgroundColor = "#eeeeee";

        document.getElementById("accordion2").style.backgroundColor = "#eeeeee";
        document.getElementById("panel2").style.backgroundColor = "#eeeeee";

        document.getElementById("accordion3").style.backgroundColor = "#eeeeee";
        document.getElementById("panel3").style.backgroundColor = "#eeeeee";
        var list1 = document.getElementsByTagName("P");
        var i;
        for(i = 0; i < list1.length; i++){
            list1[i].style.color = "black";
        }
        var list2 = document.getElementsByTagName("h3");
        for(i = 0; i < list2.length; i++){
            list2[i].style.color = "black";
        }
        var list3 = document.getElementsByTagName("h2");
        for(i = 0; i < list3.length; i++){    
            list3[i].style.color = "black";
        }
        var list4 = document.getElementsByTagName("h1");
        for(i = 0; i < list4.length; i++){
            list4[i].style.color = "black";
        }
    } else {
        document.body.style.backgroundColor = "#222222";
        document.getElementById("accordion1").style.backgroundColor = "#555555";
        document.getElementById("panel1").style.backgroundColor = "#555555";

        document.getElementById("accordion2").style.backgroundColor = "#555555";
        document.getElementById("panel2").style.backgroundColor = "#555555";

        document.getElementById("accordion3").style.backgroundColor = "#555555";
        document.getElementById("panel3").style.backgroundColor = "#555555";

        var list1 = document.getElementsByTagName("P");
        var i;
        for(i = 0; i < list1.length; i++){
            list1[i].style.color = "white";
        }
        var list2 = document.getElementsByTagName("h3");
        for(i = 0; i < list2.length; i++){
            list2[i].style.color = "white";
        }
        var list3 = document.getElementsByTagName("h2");
        for(i = 0; i < list3.length; i++){    
            list3[i].style.color = "white";
        }
        var list4 = document.getElementsByTagName("h1");
        for(i = 0; i < list4.length; i++){
            list4[i].style.color = "white";
        }
    }
}