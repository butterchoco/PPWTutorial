$(document).ready( function() {
    $("input").keyup(function () {
        email = $('#id_email').val();
        console.log(email)
        $.ajax({
            method: "POST",
            url: '/subscribe/validate/',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken) {
                    $("#alert").html('<div class="alert alert-danger" role="alert">Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain !</div>')
                    $('#submit').prop("disabled", true);
                } else {
                    $(".alert").remove()
                }
                if (($('#id_nama').val() == '' || $('#id_email').val() == '' || $('#id_password').val() == '') || data.is_taken){
                    $('#submit').prop("disabled", true);
                } else {
                    $('#submit').removeAttr('disabled');
                }
            }
        });
    });

    $("form").submit( function() {
        nama = $('#id_nama').val();
        email = $('#id_email').val();
        password = $('#id_password').val();
        $.ajax({
            method: "POST",
            url: '/subscribe/add_subscriber/',
            data: {
                'nama': nama,
                'email': email,
                'password': password
            },
            dataType: 'json',
            success: function (data) {
                console.log(true)
                $("#alert").html('<div class="alert alert-success" role="alert">Data berhasil disimpan !</div>');
                $(".alert-success").animate({opacity:"0", height:"0"}, 500)
                $('#id_nama').val("");
                $('#id_email').val("");
                $('#id_password').val("");
            }
        });
        event.preventDefault();
    })
});