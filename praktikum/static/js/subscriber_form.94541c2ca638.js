$(document).ready( function() {

    $(window).load( function(){
        $.ajax({
            method: "GET",
            url: '/subscribe/list_subscriber',
            success: function(result) {
                for(let i = 0; i < result.length; i++) {
                    var nama = result[i]['nama'];
                    var email = result[i]['email'];
                    var temp = "<thead><th>Nama</th><th>Email</th></thead>";
                    temp += "<tbody><tr class='success'><td>"+ nama + "</td>";
                    temp += "<td>" + email + "</td></tr></tbody>";
                    $("#list_subscriber").html(temp)
                }
            }
        })
    });


    $("input").keyup(function () {
        email = $('#id_email').val();
        console.log(email)
        $.ajax({
            method: "POST",
            url: '/subscribe/validate/',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken) {
                    $("#alert").html('<div class="alert alert-danger" role="alert">Email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain !</div>')
                    $('#submit').prop("disabled", true);
                } else {
                    $(".alert").remove()
                }
                if (($('#id_nama').val() == '' || $('#id_email').val() == '' || $('#id_password').val() == '') || data.is_taken){
                    $('#submit').prop("disabled", true);
                } else {
                    $('#submit').removeAttr('disabled');
                }
            }
        });
    });

    $("form").submit( function() {
        nama = $('#id_nama').val();
        email = $('#id_email').val();
        password = $('#id_password').val();
        $.ajax({
            method: "POST",
            url: '/subscribe/add_subscriber/',
            data: {
                'nama': nama,
                'email': email,
                'password': password
            },
            dataType: 'json',
            success: function (data) {
                console.log(true)
                $("#alert").html('<div class="alert alert-success" role="alert">Data berhasil disimpan !</div>');
                $(".alert-success").animate({opacity:"0"}, 2500)
                $(".alert-success").animate({height:"0"}, 1000)
                $('#id_nama').val("");
                $('#id_email').val("");
                $('#id_password').val("");
            }
        });
        event.preventDefault();
    })
});