from django.shortcuts import render
import requests
from .forms import snippet_form
from .models import schedule_model
from django.http import HttpResponseRedirect


# Create your views here.
response = {}

def schedule(request):
    schedule = schedule_model.objects.all()
    response["message"] = schedule
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    else:
        if 'name' in response.keys():
            response.__delitem__('name')
    return render(request, 'schedule.html', response)

def delete(request):
    schedule_model.objects.all().delete()
    return HttpResponseRedirect('/schedule/')

def add_schedule(request):
    if (request.method == "POST"):
        form = snippet_form(request.POST)
        if (form.is_valid):
            form.save()
            return HttpResponseRedirect('/schedule/')
    else :
        form = snippet_form()
    return render(request, 'add_schedule.html', {'form':form})
