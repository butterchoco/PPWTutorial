from django import forms
from django.forms import ModelForm
from .models import schedule_model


class snippet_form(ModelForm):
    categories = (
        ('Penting', 'Penting'),
        ('Santai', 'Santai')
    )
    jadwal = forms.DateField(widget=forms.DateTimeInput(attrs={'type': 'date', 'class': 'form-control'}), input_formats=['%Y-%m-%d'])
    jam = forms.TimeField(widget=forms.DateTimeInput(attrs={'type': 'time', 'class': 'form-control'}))
    kategori = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=categories)
    
    class Meta:
        model = schedule_model
        widgets = {'nama_kegiatan': forms.TextInput(attrs={'class': 'form-control'}), 'tempat': forms.TextInput(attrs={'class': 'form-control'})}
        fields = ['nama_kegiatan', 'jadwal', 'jam', 'tempat', 'kategori']