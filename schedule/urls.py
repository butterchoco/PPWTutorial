from django.urls import path
from .views import *

app_name = 'schedules'
# url for app
urlpatterns = [
    path('', schedule, name='schedule'),
    path('delete/', delete, name='delete'),
    path('add/', add_schedule, name='add_schedule'),
]
