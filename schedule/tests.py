from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from .views import add_schedule, delete, schedule
from .models import schedule_model
from .forms import snippet_form

class belajarfilm_testcase(TestCase):

    def test_belajarfilm_url_schedule_is_exist(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code, 200)

    def test_belajarfilm_url_add_is_exist(self):
        response = Client().get('/schedule/add/')
        self.assertEqual(response.status_code, 200)

    def test_belajarfilm_url_delete_schedule_is_exist(self):
        response = Client().get('/schedule/delete/')
        self.assertEqual(response.status_code, 302)

    def test_belajarfilm_using_add_function(self):
        found = resolve('/schedule/add/')
        self.assertEqual(found.func, add_schedule)

    def test_belajarfilm_using_delete_function(self):
        found = resolve('/schedule/delete/')
        self.assertEqual(found.func, delete)

    def test_belajarfilm_using_schedule_function(self):
        found = resolve('/schedule/')
        self.assertEqual(found.func, schedule)

    def test_model_can_create_new_schedule(self):
        schedule_model.objects.create(nama_kegiatan='Makan', jadwal='2000-04-04', jam='00:00', tempat='jalanan', kategori='Penting')
        counting_all_available_schedule = schedule_model.objects.all().count()
        self.assertEqual(counting_all_available_schedule, 1)

    def test_form_validation_for_blank_items(self):
        form = snippet_form(data={'nama_kegiatan':'', 'jadwal':'', 'jam':'', 'tempat':'', 'kategori':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama_kegiatan'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['jadwal'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['jam'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['tempat'],
            ["This field is required."],
        )
        self.assertEqual(
            form.errors['kategori'],
            ["This field is required."],
        )


    def test_belajarfilm_post_success_and_render_the_result(self):
        test = 'apa aja'
        response_post = Client().post('/schedule/add/', {'nama_kegiatan':test, 'jadwal':'2000-04-04', 'jam':'00:00', 'tempat':test, 'kategori':'Penting'})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/schedule/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_belajarfilm_post_error_and_render_the_result(self):
        test = 'apa saja' * 300
        Client().post("/add/", {'nama_kegiatan':test, 'jadwal':'2000-04-04', 'jam':'00:00', 'tempat':test, 'kategori':'Penting'})
        self.assertEqual(schedule_model.objects.filter(nama_kegiatan=test).count(), 0)

# class PPW_testCase(TestCase):
#     """docstring for belajarfilm_functional_test"""

#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(PPW_testCase, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(PPW_testCase, self).tearDown()

#     def test_input_todo(self):
#         selenium = self.selenium
#         selenium.get('http://ppw-c-belajarfilm.herokuapp.com/about')
#         skill = selenium.find_element_by_id('accordion1')
#         theme = selenium.find_element_by_id('theme')
#         more = selenium.find_element_by_class_name('navbar-toggler')
#         time.sleep(1)
#         skill.click()
#         time.sleep(4)
#         more.click()
#         time.sleep(2)
#         theme.click()
#         time.sleep(4)