from django.shortcuts import render
import requests
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse

response = {}
# Create your views here.


def json(request):
    keyword = request.GET.get("find", "hacker")
    r = requests.get(
        url="https://www.googleapis.com/books/v1/volumes?q=" + keyword)
    data = r.json()
    return JsonResponse(data)


def my_library(request):
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    else:
        if 'name' in response.keys():
            response.__delitem__('name')
    return render(request, 'library.html', response)

def signout(request):
    request.session.flush()
    return HttpResponse('sign out')

@csrf_exempt
def authenticate(request):
    if (request.method == "POST"):
        TOKEN = request.POST['token']
        VALIDATE_TOKEN = requests.get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="+TOKEN)
        user_data = VALIDATE_TOKEN.json()
        request.session["name"] = user_data["name"]
        request.session["email"] = user_data["email"]
        request.session["picture"] = user_data["picture"]
        return JsonResponse(user_data)
    return HttpResponse("User Sign in")
