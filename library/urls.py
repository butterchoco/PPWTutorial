from django.urls import path
from .views import *

app_name = 'library'
# url for app
urlpatterns = [
    path('', my_library, name='my_library'),
    path('json', json, name='json'),
    path('authenticate', authenticate, name='authenticate'),
    path('signing_out', signout, name='signout'),
]
