from django.shortcuts import render

# Create your views here.
response = {}
def about(request):
    if 'name' in request.session.keys():
        response['name'] = request.session['name']
    else:
        if 'name' in response.keys():
            response.__delitem__('name')
    return render(request, 'about.html', response)