from django.urls import path
from .views import *

app_name = 'about'
# url for app
urlpatterns = [
    path('', about, name='about'),
]
