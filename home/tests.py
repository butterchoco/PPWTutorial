from django.test import TestCase, Client
from django.urls import resolve
from .views import home

# Create your tests here.

class belajarfilm_home_testcase(TestCase):
    def test_belajarfilm_url_home_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)
    
    def test_belajarfilm_using_home_function(self):
        found = resolve('/home/')
        self.assertEqual(found.func, home)