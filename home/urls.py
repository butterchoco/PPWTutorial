from django.urls import path
from .views import *

app_name = 'home'
# url for app
urlpatterns = [
    path('', home, name='home'),
]
